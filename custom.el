(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("9d5124bef86c2348d7d4774ca384ae7b6027ff7f6eb3c401378e298ce605f83a" "dccf4a8f1aaf5f24d2ab63af1aa75fd9d535c83377f8e26380162e888be0c6a9" "4ade6b630ba8cbab10703b27fd05bb43aaf8a3e5ba8c2dc1ea4a2de5f8d45882" "b754d3a03c34cfba9ad7991380d26984ebd0761925773530e24d8dd8b6894738" "6945dadc749ac5cbd47012cad836f92aea9ebec9f504d32fe89a956260773ca4" default))
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(org-agenda-files
   '("~/.config/doom/config.org" "/home/vrakfall/org/roam/curienstein-private/resource/20240528142739-main_todo_list.org" "/home/vrakfall/org/roam/curienstein-public/resource/20230309033411-org_mode.org" "/home/vrakfall/org/roam/curienstein-public/resource/20230426130416-wil_wheaton.org" "/home/vrakfall/org/roam/daily/2023-04-06.org" "/home/vrakfall/org/roam/daily/2023-03-23.org" "/home/vrakfall/org/job-hunt.org" "/home/vrakfall/org/main.org"))
 '(safe-local-variable-values
   '((dirvish-emerge-groups
      ("Text"
       (extensions "md txt org")
       nil nil)
      ("Images"
       (extensions "jpg png")
       nil nil)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ts-fold-replacement-face ((t (:foreground nil :box nil :inherit font-lock-comment-face :weight light)))))
